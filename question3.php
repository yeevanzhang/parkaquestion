<?php
function my_arraydepth($arr){
  if (!is_array($arr)){
    return 0;
  }
  $max_layers = 1;
  foreach ($arr as $value){
    if (is_array($value)){
      $i = my_arraydepth($value) + 1;
      if ($i > $max_layers){
        $max_layers = $i;
      }
    }
  }
  return $max_layers;
}