<?php
function my_checkplain($comments){
  $str = trim($comments);
  if ($str == ''){
    return 0;
  }else{
    //keep html tags "a,b,i,img"
    $newStr = strip_tags($str,'<a><b><i><img>');
    return htmlspecialchars($newStr, ENT_QUOTES,'UTF-8');
  }
}