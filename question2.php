CREATE TABLE `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(250) NOT NULL,
  `price` decimal(11,0) NOT NULL,
  `category_id` int(11) unsigned NOT NULL,
  `type_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `categoriesid001` (`category_id`),
  KEY `typeid001` (`type_id`),
  CONSTRAINT `categoriesid001` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  CONSTRAINT `typeid001` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;


CREATE TABLE `type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `typename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;


CREATE TABLE `categories` (
  `id` int(11) unsigned NOT NULL,
  `parentid` int(11) NOT NULL,
  `categoryname` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1

//To get all items from category 'X'

$sql1 = 'SELECT a.name, a.description, a.price 
FROM products AS a
LEFT JOIN categories AS b
ON a.`category_id` = b.`id`
WHERE b.`id`=20000

//To get all items from category 'X' including all sub-categories
$sql2 = 'SELECT a.name, a.description, a.price 
FROM products AS a
LEFT JOIN categories AS b
ON a.`category_id` = b.`id`
WHERE b.`id`=20000 OR b.`parentid`=20000';

